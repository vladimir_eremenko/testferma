package task1.models;

public class Fermer {

	private Ferma ferma;

	public void addCow(Cow cow){
		ferma.getCowshed().put(cow.getCowId(), cow);
	}

	public void deleteCow(int id){
		ferma.getCowshed().remove(id);
	}

	public Ferma getFerma() {
		return ferma;
	}

	public void setFerma(Ferma ferma) {
		this.ferma = ferma;
	}
}
