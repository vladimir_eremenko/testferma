package task1;

import task1.models.Cow;
import task1.models.Ferma;
import task1.models.Fermer;

public class Main {

	public static void main(String[] args) {

		Fermer fermer = new Fermer();
		Ferma ferma = new Ferma();
		fermer.setFerma(ferma);

		fermer.addCow(new Cow(123, "eva"));

		Cow cow = fermer.getFerma().getCowshed().get(123).giveBirth(111, "bonny");
		fermer.addCow(cow);
		//fermer.deleteCow(cow.endLifeSpan(2));

		View view = new View();
		view.showFerma(fermer.getFerma());
	}
}
