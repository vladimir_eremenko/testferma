package by.mbicycle.test.actions;

public class ActionMinus implements IAction{
	@Override
	public Double makeAction(Double a, Double b) {
		return a - b;
	}
}
