package by.mbicycle.test.actions;

public interface IAction {
	Double makeAction (Double a, Double b);
}
