package by.mbicycle.test;


import by.mbicycle.test.calculators.Calculator;
import by.mbicycle.test.calculators.ICalculator;
import by.mbicycle.test.converter.Converter;
import by.mbicycle.test.converter.IConverter;
import by.mbicycle.test.readers.IReader;
import by.mbicycle.test.readers.ScannerReader;

public class Main {
	public static void main(String[] args) {

		IReader reader = new ScannerReader();

		View view = new View();
		view.setDate(reader);

		IConverter converter = new Converter();
		ICalculator colculator = new Calculator();


		try {
			converter.convert(view.getDate());
			colculator.calculate(converter.getResult());
			view.showResult(String.valueOf(colculator.getResult()));
		} catch (Exception e) {
			view.showResult(e.getMessage());
		}
	}
}
