package by.mbicycle.test.logic;

import static by.mbicycle.test.constants.Constants.*;

public class AbstractLogic {

	protected String getOperationString(char c) {
		String result = EMPTY;
		switch (c) {
			case MINUS_CHAR:
				result = MINUS;
				break;
			case PLUS_CHAR:
				result = PLUS;
				break;
			case MULTIPLY_CHAR:
				result = MULTIPLY;
				break;
			case DIVIDE_CHAR:
				result = DIVIDE;
				break;
		}
		return result;
	}

	protected boolean isOp(char c) {
		return c == MINUS_CHAR || c == PLUS_CHAR || c == MULTIPLY_CHAR || c == DIVIDE_CHAR;
	}


	protected  int opPrior(char op) {
		return op == MULTIPLY_CHAR || op == DIVIDE_CHAR ? TWO : ONE;
	}

}
