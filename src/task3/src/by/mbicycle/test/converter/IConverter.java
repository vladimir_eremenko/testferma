package by.mbicycle.test.converter;

public interface IConverter {

	void convert(String value) throws Exception;
	String getResult();
}
