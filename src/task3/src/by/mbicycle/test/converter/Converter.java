package by.mbicycle.test.converter;

import by.mbicycle.test.logic.AbstractLogic;
import by.mbicycle.test.constants.Constants;

public class Converter extends AbstractLogic implements IConverter{

	private String result;

	public void convert(String value) throws Exception {
		StringBuilder sbStack = new StringBuilder(Constants.EMPTY), sbOut = new StringBuilder(Constants.EMPTY);
		char cIn, cTmp;

		for (int i = Constants.ZERO; i < value.length(); i++) {
			cIn = value.charAt(i);
			if (isOp(cIn)) {
				while (sbStack.length() > Constants.ZERO) {
					cTmp = sbStack.substring(sbStack.length() - Constants.ONE).charAt(Constants.ZERO);
					if (isOp(cTmp) && (opPrior(cIn) <= opPrior(cTmp))) {
						sbOut.append(Constants.SPACE).append(cTmp).append(Constants.SPACE);
						sbStack.setLength(sbStack.length() - Constants.ONE);
					} else {
						sbOut.append(Constants.SPACE);
						break;
					}
				}
				sbOut.append(Constants.SPACE);
				sbStack.append(cIn);
			} else if (Constants.OPEN_BRACKET_CHAR == cIn) {
				sbStack.append(cIn);
			} else if (Constants.CLOSE_BRACKET_CHAR == cIn) {
				cTmp = sbStack.substring(sbStack.length() - 1).charAt(Constants.ZERO);
				while (Constants.OPEN_BRACKET_CHAR != cTmp) {
					if (sbStack.length() < Constants.ONE) {
						throw new Exception("Bracket parsing error. Verify that the expression is valid.");
					}
					sbOut.append(Constants.SPACE).append(cTmp);
					sbStack.setLength(sbStack.length() - Constants.ONE);
					cTmp = sbStack.substring(sbStack.length() - Constants.ONE).charAt(Constants.ZERO);
				}
				sbStack.setLength(sbStack.length() - Constants.ONE);
			} else {
				sbOut.append(cIn);
			}
		}

		while (sbStack.length() > Constants.ZERO) {
			sbOut.append(Constants.SPACE).append(sbStack.substring(sbStack.length() - Constants.ONE));
			sbStack.setLength(sbStack.length() - Constants.ONE);
		}
		setResult(sbOut.toString());
	}

	public String getResult() {
		return result;
	}

	private void setResult(String result) {
		this.result = result;
	}


}
