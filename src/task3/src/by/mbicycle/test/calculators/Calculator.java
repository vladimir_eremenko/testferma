package by.mbicycle.test.calculators;

import by.mbicycle.test.logic.AbstractLogic;
import by.mbicycle.test.constants.Constants;
import by.mbicycle.test.factories.ActionFactory;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class Calculator extends AbstractLogic implements ICalculator {

	private double result;
	private Deque<Double> stack = new ArrayDeque<>();

	public void calculate(String value) throws Exception {
		String sTe;
		StringTokenizer st = new StringTokenizer(value);

		while (st.hasMoreTokens()) {
			try {
				sTe = st.nextToken().trim();
				stack = choiceOfAction(sTe);
			} catch (Exception e) {
				throw new Exception("Invalid character in expression.", e);
			}
		}
		errorOperands(stack);
		setResult(stack.pop());
	}

	private void errorOperands(Deque<Double> stack) throws Exception {
		if (stack.size() > Constants.ONE) {
			throw new Exception("The number of operators does not match the number of operands.");
		}
	}

	private void errorDate(Deque<Double> stack) throws Exception {
		if (stack.size() < Constants.TWO) {
			throw new Exception("The number of data in the stack for the operation is incorrect.");
		}
	}

	private Deque<Double> choiceOfAction(String value) throws Exception {
		double dA;
		double dB;
		if (Constants.ONE == value.length() && isOp(value.charAt(Constants.ZERO))) {
			errorDate(stack);
			dB = stack.pop();
			dA = stack.pop();
			dA = ActionFactory.valueOf(getOperationString(value.charAt(Constants.ZERO)))
					.getInstance().makeAction(dA, dB);
			stack.push(dA);
		} else {
			dA = Double.parseDouble(value);
			stack.push(dA);
		}
		return stack;
	}

	public double getResult() {
		return result;
	}

	private void setResult(double result) {
		this.result = result;
	}

}
