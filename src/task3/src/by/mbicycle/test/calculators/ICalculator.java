package by.mbicycle.test.calculators;

public interface ICalculator {
	void calculate(String value) throws Exception;
	double getResult();
}
