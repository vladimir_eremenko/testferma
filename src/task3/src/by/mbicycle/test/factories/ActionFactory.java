package by.mbicycle.test.factories;

import by.mbicycle.test.actions.*;

public enum ActionFactory {

	MINUS {
		@Override
		public IAction getInstance() {
			return new ActionMinus();
		}
	},
	PLUS {
		@Override
		public IAction getInstance() {
			return new ActionPlus();
		}
	},
	MULTIPLY {
		@Override
		public IAction getInstance() {
			return new ActionMultiply();
		}
	},
	DIVIDE {
		@Override
		public IAction getInstance() {
			return new ActionDivide();
		}
	};


	public abstract IAction getInstance();
}
