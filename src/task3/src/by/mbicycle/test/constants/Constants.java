package by.mbicycle.test.constants;

public class Constants {

	public static final String MINUS = "MINUS";
	public static final String PLUS = "PLUS";
	public static final String MULTIPLY = "MULTIPLY";
	public static final String DIVIDE = "DIVIDE";

	public static final char MINUS_CHAR = '-';
	public static final char PLUS_CHAR = '+';
	public static final char MULTIPLY_CHAR = '*';
	public static final char DIVIDE_CHAR = '/';

	public static final char OPEN_BRACKET_CHAR = '(';
	public static final char CLOSE_BRACKET_CHAR = ')';



	public static final String EMPTY = "";
	public static final String SPACE = " ";
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int ZERO = 0;

}
