package by.mbicycle.test.readers;

public interface IReader {
	String getInput();
}
