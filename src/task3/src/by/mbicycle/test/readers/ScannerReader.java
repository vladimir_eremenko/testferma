package by.mbicycle.test.readers;

import java.util.Scanner;

public class ScannerReader implements IReader{

	private Scanner sc = new Scanner(System.in);

	@Override
	public String getInput() {
		return sc.nextLine();
	}
}
