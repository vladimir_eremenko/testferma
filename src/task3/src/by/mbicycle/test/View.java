package by.mbicycle.test;

import by.mbicycle.test.readers.IReader;

public class View {

	private String date;

    public void showResult(String result){
		System.out.println("Result ");
		System.out.println(result);
	}

	public String getDate() {
		return date;
	}

	public void setDate(IReader reader) {
		System.out.println("Enter date ");
		this.date = reader.getInput();
	}
}
