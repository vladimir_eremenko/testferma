package task2.models;

public class Cow {

	private int cowId;
	private String nickName;
	private int parentCowId;

	public Cow() {
	}

	public Cow(int cowId, String nickName) {
		this.cowId = cowId;
		this.nickName = nickName;
	}

	private Cow( int parentCowId, int cowId, String nickName) {
		this.cowId = cowId;
		this.nickName = nickName;
		this.parentCowId = parentCowId;
	}

	public Cow giveBirth(int childCowId, String childNickName) {
		return new Cow(cowId, childCowId, childNickName);
	}

	public int endLifeSpan(int cowId) {
		return cowId;
	}

	public int getCowId() {
		return cowId;
	}

	public void setCowId(int cowId) {
		this.cowId = cowId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getParentCowId() {
		return parentCowId;
	}

	public void setParentCowId(int parentCowId) {
		this.parentCowId = parentCowId;
	}

	@Override
	public String toString() {
		return "Cow{" +
				"cowId=" + cowId +
				", nickName='" + nickName + '\'' +
				", parentCowId=" + parentCowId +
				'}' + "\n";
	}
}
