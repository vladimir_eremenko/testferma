package task2.models;

public class Fermer {

	private Ferma ferma;

	public void addCow(Cow cow){
		ferma.getCowshed().add(cow);
	}

	public void deleteCow(int id){
		ferma.getCowshed().remove(id);
	}

	public Ferma getFerma() {
		return ferma;
	}

	public void setFerma(Ferma ferma) {
		this.ferma = ferma;
	}
}
