package task2.collection;

import java.util.*;

public class ELinkedList<N> implements IList<N>, Iterable<N> {

	private Node node;

	public class Node<N> {
		private Node<N> prev;
		private N type;
		private Node<N> next;

		Node(N input) {
			prev = null;
			type = input;
			next = null;
		}

		Node(N input, Node<N> current) {
			prev = current;
			type = input;

			next = current.next;
			current.next = this;

			if (next != null) {
				next.prev = this;
			}
		}

		@Override
		public String toString() {
			return type + "";
		}
	}

	private int size;
	private Node<N> last;
	private Node<N> first;

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean empty() {
		boolean check = false;
		if (size == 0) {
			check = true;
		}
		return check;
	}

	public boolean add(N input) {
		Node<N> n = new Node<N>(input);
		if (empty()) {
			first = n;
			last = first;
		} else {
			last = new Node<N>(input, last);
		}
		size++;
		return true;
	}

	public boolean add(int index, N input) {
		checkObjectAndIndex(index, input);
		if (index > size - 1) {
			add(input);
			return true;
		} else if (index == 0) {
			Node<N> node = new Node<N>(input);

			first.prev = node;
			node.next = first;
			first = node;
			size++;
			return true;
		} else {
			Node<N> node = getNode(index - 1);
			node = new Node<N>(input, node);
			size++;
			return true;
		}
	}

	public Node<N> getNode(int index) {
		if (index >= size - 1)
			return last;
		if (index == 0)
			return first;
		Node<N> node = first;
		for (int i = 0; i < index; i++)
			node = node.next;
		return node;
	}

	boolean addNode(Node<N> node, N input) {
		Node<N> nNode = new Node<>(input, node);
		if (empty()) {
			first = nNode;
			last = first;
			size++;
			return true;
		} else {
			last.next = nNode;
			size++;
			return true;
		}
	}


	@Override
	public boolean contains(Object e) {
		boolean b = false;
		for (int i = 0; i < size; i++) {
			if (get(i).equals(e)) b = true;
		}
		return b;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public N get(int index) {
		N element;
		if (index >= 0 && index < size()) {
			element = getNode(index).type;
		} else throw new IndexOutOfBoundsException();
		return element;
	}

	@Override
	public void sort(Comparator<N> comp) {
		for (int i = size - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (comp.compare(getNode(j).type, getNode(j + 1).type) > 0) {
					Node<N> node = getNode(j);
					removeNode(getNode(j));
					add(j + 1, node.type);
				}
			}
		}
	}

	@Override
	public boolean remove(int index) {
		checkIndex(index);
		nodeDelete(getNode(index));
		size--;
		return true;
	}

	public boolean remove(Object o) {
		boolean b = false;
		for (int i = 0; i < size(); i++)
			if (getNode(i).type.equals(o)) b = remove(i);
		return b;
	}

	void removeNode(Node<N> node) throws IllegalArgumentException {
		checkNullNode(node);
		nodeDelete(node);
		size--;
	}

	@Override
	public int indexOf(N o) {
		for (int i = 0; i < size(); i++) {
			if (get(i).equals(o)) {
				return i;
			}
		}
		return -1;
	}

	private void checkNullNode(Node<N> node) throws IllegalArgumentException {
		if (node == null) {
			throw new IllegalArgumentException("Node is NULL");
		}
	}

	private void checkObjectAndIndex(int index, N input) throws IllegalArgumentException {
		if (input == null || index < 0) throw new IllegalArgumentException();
	}

	private void checkIndex(final int index) {
		if (index > size || index < 0) {
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
		}
	}

	private void nodeDelete(Node<N> node) {
		if (size != 1) {
			Node<N> nodeBefore = node.prev;
			Node<N> nodeAfter = node.next;

			if (nodeBefore == null) {
				first = nodeAfter;
				nodeAfter.prev = null;
			} else if (nodeAfter == null) {
				last = nodeBefore;
				nodeBefore.next = null;
			} else {
				nodeBefore.next = nodeAfter;
				nodeAfter.prev = nodeBefore;
			}
		} else {
			first = null;
			last = null;
		}
	}

	public Iterator<N> iterator() {
		return new LinkedListIterator();
	}

	private class LinkedListIterator implements Iterator<N> {
		private Node n = first;

		public N next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			N item = (N) n.type;
			n = n.next;
			return item;
		}

		public boolean hasNext() {
			return n != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (N item : this)
			s.append(item + " ");
		return s.toString();
	}
}