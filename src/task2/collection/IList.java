package task2.collection;

import java.util.Comparator;

public interface IList<Type> {

    int size();

    boolean empty();

    boolean contains(Object o);

    boolean add(Type e);

    boolean add(int index, Type element);

    void clear();

    Type get(int index);

    void sort(Comparator<Type> comp);

    boolean remove(int index);

    int indexOf(Type o);

    boolean remove(Object o);
}
